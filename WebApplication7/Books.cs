﻿public class Books
{
    public string Name { get; set; } = "";
    public string Author { get; set; } = "";
    public int Published { get; set; }
}
