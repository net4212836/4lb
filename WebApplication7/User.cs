﻿public class User
{
    public int Id { get; set; }
    public string Name { get; set; } = "";
    public int BooksTaken { get; set; }
    public int YearOfBirth { get; set; }
}
