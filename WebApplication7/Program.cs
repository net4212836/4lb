using System.Collections.Generic;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

app.MapGet("/Library", () => "Hello! And welcome to the Los Pollos Hermanos family. My name is Gustavo, but you can call me Gus.");
builder.Configuration.AddJsonFile("BookList.json");
app.MapGet("/Library/Books", async (context) =>
{

    List<Books> books = builder.Configuration.GetSection("books").Get<List<Books >>();
    context.Response.ContentType = "text/html;charset=utf-8";
    await context.Response.WriteAsync("<h1>All books in configuration</h1>");
    await context.Response.WriteAsync("<table>");
    await context.Response.WriteAsync("<tr><th>Name</th><th>Author</th><th>Year of publication</th></tr>");
    foreach (var book in books)
    {
        await context.Response.WriteAsync("<tr>");
        await context.Response.WriteAsync("<td>");
        await context.Response.WriteAsync(book.Name);
        await context.Response.WriteAsync("</td>");
        await context.Response.WriteAsync("<td>");
        await context.Response.WriteAsync(book.Author);
        await context.Response.WriteAsync("</td>");
        await context.Response.WriteAsync("<td>");
        await context.Response.WriteAsync(book.Published.ToString());
        await context.Response.WriteAsync("</td>");
        await context.Response.WriteAsync("</tr>");
    }
    await context.Response.WriteAsync("</table>");
});

builder.Configuration.AddJsonFile("Users.json");

app.MapGet("/Library/Profile/{id:int:range(1, 5)?}", (int? id) =>
{
    List<User> users = builder.Configuration.GetSection("users").Get<List<User>>();
    if (id == null)
    {
        return $"name:{users[0].Name}, number of taken books: {users[0].BooksTaken}, year of birth: {users[0].YearOfBirth}";
    }
        return $"name:{users[id.Value-1].Name}, number of taken books: {users[id.Value - 1].BooksTaken}, year of birth: {users[id.Value - 1].YearOfBirth}"; 
});

app.Run();
